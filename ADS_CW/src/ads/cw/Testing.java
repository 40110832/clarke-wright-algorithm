package ads.cw;

public class Testing {
	private static VRSolution s;
	private static VRProblem p;

	public static void main(String[] args) throws Exception {

		p = new VRProblem("E:\\CW_data\\rand01000prob.csv");
		// Load problem
		s = new VRSolution(p);
		// Create blank solution

	//	for (int i = 1; i <= 10; i++) { 
			double startTime = System.currentTimeMillis();
			// Time started
			s.Clarke_Wright();
			// Use the existing problem solver to build a solution
			double endTime = System.currentTimeMillis();
			// Time finished
			System.out.println("The time taken was " + (endTime - startTime));
	//	}

		System.out.println("Cost =" + s.solnCost());
		// Print out the cost of the solution
		s.writeOut("E:\\CW_sol\\rand01000sol.csv");
		// Save the solution file for verification
		s.writeSVG("E:\\CW_sol\\rand01000.svg", "E:\\CW_sol\\rand01000sol.svg");
		// Create pictures of the problem and the solution

	}
}
