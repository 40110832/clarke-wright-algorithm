package ads.cw;

import java.util.*;
import java.io.*;

public class VRSolution {
	public VRProblem prob;
	public List<List<Customer>> soln = new ArrayList<List<Customer>>();

	public VRSolution(VRProblem problem) {
		this.prob = problem;
	}

	// The dumb solver adds one route per customer
	public void oneRoutePerCustomerSolution() {
		this.soln = new ArrayList<List<Customer>>();
		for (Customer c : prob.customers) {
			ArrayList<Customer> route = new ArrayList<Customer>();
			route.add(c);
			soln.add(route);
		}
	}
    //Clarke-Wright Solution
	public void Clarke_Wright() {

		this.soln = new ArrayList<List<Customer>>();
		ArrayList<Route> pairs = new ArrayList<Route>();		
		ArrayList<Customer> leftcusts = new ArrayList<Customer>();
		leftcusts.addAll(prob.customers);//add all customers 
				
			// calculate the savings of all the pairs				
		for (int i = 0; i < prob.customers.size(); i++) {
			for (int j = i+1; j < prob.customers.size(); j++) { 
					Route route = new Route(prob.depot.c);
					route.add(prob.customers.get(i));
					route.add(prob.customers.get(j));
					pairs.add(route);				
			}
		}

		Collections.sort(pairs); // order pairs by savings

		ArrayList<Route> allroutes = new ArrayList<Route>();
		allroutes.add(pairs.get(0));//add first pair 
		pairs.remove(0);//remove 1st

		//  combining pairs into routes
		mainloop: for (int pair = 0; pair < pairs.size(); pair++) { //go through all pairs
			Route rt = pairs.get(pair); //add next pair
			Customer cust1 = rt.customers.get(0); // first customer
			Customer cust2 = rt.customers.get(rt.customers.size() - 1);// second customer
																	
			
			for (Route ro : allroutes) {//for all lists of customers
				Customer fcus = ro.customers.get(0); //add first customer
				Customer lcus = ro.customers.get(ro.customers.size() - 1); //add last customer
				
				//check the two combinations
				boolean check = false;
				for (int i = 1; i <= 2; i++) {
					check = !check;
					Customer p1, p2;
					if (check) {
						p1 = cust1;
						p2 = cust2;
					} else {
						p1 = cust2;
						p2 = cust1;
					}

					// if there is common link between 
					//the pair and the lcust or fcust in
					//the current route
					if (p1 == fcus || p1 == lcus) {
						// general requirement must not exceed 50 
						if (p2.c + ro.getRequirment() <= prob.depot.c) {
							// If the route doesn't contain this customer
							if (!ro.customers.contains(p2)) {
								// and the customer is not in another route
								boolean inanotherroute = false;
								for (Route rr : allroutes) {
									if (rr.customers.contains(p2)) {
										inanotherroute = true;
										break;
									}
								}
								if (!inanotherroute) {
									// Now we can add the customer
									//at the end or beginning of current route
									if (cust1 == fcus) {
										ro.addcustomer(p2, true);										
									} else {
										ro.addcustomer(p2, false);
									}
								}
							}
							
							leftcusts.remove(p2);
							pairs.remove(rt);
							pair--;//move to next pair														
							continue mainloop;
						}
					}
				}
			}

			// ...current pair is not added to any route
			boolean check1 = false;
			boolean check2 = false;
			for (Route route : allroutes) {
				//check that cust1 is not in any route
				if (route.customers.contains(cust1)) {
					check1 = true;
				}
				//check that cust2 is not in any route
				if (route.customers.contains(cust2)) {
					check2 = true;
				}
			}
			//create new route if both are not in any route
			if (!(check1 || check2)) {
				leftcusts.remove(cust1);
				leftcusts.remove(cust2);
				allroutes.add(rt); 
			}
			pairs.remove(rt);
			pair--; //move to next pair

		}

		// for all lists of customers in allroutes, add them to new Arraylist
				for (Route route : allroutes) {
					ArrayList<Customer> list = new ArrayList<Customer>();
					list.addAll(route.customers); //add the list to soln
					soln.add(list);
				}
				
		//for all customers in left list
		for (Customer cust : leftcusts) {
	   //add left customers to new route
			ArrayList<Customer> leftcustomer = new ArrayList<Customer>();
			leftcustomer.add(cust);
			soln.add(leftcustomer);//add it to soln
		}		
	}

	// Calculate the total journey
	public double solnCost() {
		double cost = 0;
		for (List<Customer> route : soln) {
			Customer prev = this.prob.depot;
			for (Customer c : route) {
				cost += prev.distance(c);
				prev = c;
			}
			// Add the cost of returning to the depot
			cost += prev.distance(this.prob.depot);
		}
		return cost;
	}

	public Boolean verify() {
		// Check that no route exceeds capacity
		Boolean okSoFar = true;
		for (List<Customer> route : soln) {
			// Start the spare capacity at
			int total = 0;
			for (Customer c : route)
				total += c.c;
			if (total > prob.depot.c) {
				System.out.printf(
						"********FAIL Route starting %s is over capacity %d\n",
						route.get(0), total);
				okSoFar = false;
			}
		}
		// Check that we keep the customer satisfied
		// Check that every customer is visited and the correct amount is picked
		// up
		Map<String, Integer> reqd = new HashMap<String, Integer>();
		for (Customer c : this.prob.customers) {
			String address = String.format("%fx%f", c.x, c.y);
			reqd.put(address, c.c);
		}
		for (List<Customer> route : this.soln) {
			for (Customer c : route) {
				String address = String.format("%fx%f", c.x, c.y);
				if (reqd.containsKey(address))
					reqd.put(address, reqd.get(address) - c.c);
				else
					System.out.printf("********FAIL no customer at %s\n",
							address);
			}
		}
		for (String address : reqd.keySet())
			if (reqd.get(address) != 0) {
				System.out.printf(
						"********FAIL Customer at %s has %d left over\n",
						address, reqd.get(address));
				okSoFar = false;
			}
		return okSoFar;
	}

	public void readIn(String filename) throws Exception {
		BufferedReader br = new BufferedReader(new FileReader(filename));
		String s;
		this.soln = new ArrayList<List<Customer>>();
		while ((s = br.readLine()) != null) {
			ArrayList<Customer> route = new ArrayList<Customer>();
			String[] xycTriple = s.split(",");
			for (int i = 0; i < xycTriple.length; i += 3)
				route.add(new Customer((int) Double.parseDouble(xycTriple[i]),
						(int) Double.parseDouble(xycTriple[i + 1]),
						(int) Double.parseDouble(xycTriple[i + 2])));
			soln.add(route);
		}
		br.close();
	}

	public void writeSVG(String probFilename, String solnFilename)
			throws Exception {
		String[] colors = "chocolate cornflowerblue crimson cyan darkblue darkcyan darkgoldenrod"
				.split(" ");
		int colIndex = 0;
		String hdr = "<?xml version='1.0'?>\n"
				+ "<!DOCTYPE svg PUBLIC '-//W3C//DTD SVG 1.1//EN' '../../svg11-flat.dtd'>\n"
				+ "<svg width='8cm' height='8cm' viewBox='0 0 500 500' xmlns='http://www.w3.org/2000/svg' version='1.1'>\n";
		String ftr = "</svg>";
		StringBuffer psb = new StringBuffer();
		StringBuffer ssb = new StringBuffer();
		psb.append(hdr);
		ssb.append(hdr);
		for (List<Customer> route : this.soln) {
			ssb.append(String.format("<path d='M%s %s ", this.prob.depot.x,
					this.prob.depot.y));
			for (Customer c : route)
				ssb.append(String.format("L%s %s", c.x, c.y));
			ssb.append(String.format(
					"z' stroke='%s' fill='none' stroke-width='2'/>\n",
					colors[colIndex++ % colors.length]));
		}
		for (Customer c : this.prob.customers) {
			String disk = String
					.format("<g transform='translate(%.0f,%.0f)'>"
							+ "<circle cx='0' cy='0' r='%d' fill='pink' stroke='black' stroke-width='1'/>"
							+ "<text text-anchor='middle' y='5'>%d</text>"
							+ "</g>\n", c.x, c.y, 10, c.c);
			psb.append(disk);
			ssb.append(disk);
		}
		String disk = String
				.format("<g transform='translate(%.0f,%.0f)'>"
						+ "<circle cx='0' cy='0' r='%d' fill='pink' stroke='black' stroke-width='1'/>"
						+ "<text text-anchor='middle' y='5'>%s</text>"
						+ "</g>\n", this.prob.depot.x, this.prob.depot.y, 20,
						"D");
		psb.append(disk);
		ssb.append(disk);
		psb.append(ftr);
		ssb.append(ftr);
		PrintStream ppw = new PrintStream(new FileOutputStream(probFilename));
		PrintStream spw = new PrintStream(new FileOutputStream(solnFilename));
		ppw.append(psb);
		spw.append(ssb);
		ppw.close();
		spw.close();
	}

	public void writeOut(String filename) throws Exception {
		PrintStream ps = new PrintStream(filename);
		for (List<Customer> route : this.soln) {
			boolean firstOne = true;
			for (Customer c : route) {
				if (!firstOne)
					ps.print(",");
				firstOne = false;
				ps.printf("%f,%f,%d", c.x, c.y, c.c);
			}
			ps.println();
		}
		ps.close();
	}
}
