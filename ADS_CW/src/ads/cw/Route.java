package ads.cw;
import java.util.*;

public class Route implements Comparable<Route> {
	private short requirement;
	private double savings;
	protected ArrayList<Customer> customers;

	private void calculate() {
		double distance = 0;// distance from depot to first and second customer
		double distancebtw = 0;// distance between two customers
		double tempdistance = 0;// temporary cost of first or second cust. to depot
		Customer previous = null;

		// Calculate the following calculation for each customer
		for (Customer current : customers) {
		// Distance from Depot
		// using Pythagorean theorem to find the distance from Depot
		  tempdistance = Math.sqrt((current.x * current.x) +
				  (current.y * current.y)); 			
		  distance += tempdistance; // add the calculated distance from depot
									  // to customer to distance variable

			if (previous != null) {// Calculate the distance from previous
								   // customer to this customer
				double x, y;
				x = (previous.x - current.x);
				y = (previous.y - current.y);
		 // using Pythagorean theorem to find the distance between two customers
				distancebtw = Math.sqrt((x * x) + (y * y)); 		
			} else {
				// there will be no change if only first customer is added
				distancebtw += tempdistance;
			}
			previous = current; // current customer becomes previous customer
		}
		savings = distance - distancebtw; // calculating the savings
	}

	protected Route(int truckcapacity) { // constructor
		@SuppressWarnings("unused")
		int cap = truckcapacity;
		customers = new ArrayList<Customer>();
		savings = 0;
	}

	protected void add(Customer cust) { // adding pairs
		customers.add(cust);
		requirement += cust.c;
		calculate();
	}

	protected void addcustomer(Customer cust, boolean place) {
		// Do we have to add the customer at the beginning or end of the route?
		if (place) {
			customers.add(0, cust); // add customer to the start of the route
		} else {
			customers.add(cust);// add customer to the end of the route
		}
     // adding the requirement of just added customer to  whole requirement
		requirement += cust.c; 							
	}

	protected double getSavings() {
		return savings;
	}

	protected int getRequirment() {
		return requirement;
	}

	public int compareTo(Route o) { // comparing savings
		return Double.compare(o.getSavings(), this.savings);
	}
}
